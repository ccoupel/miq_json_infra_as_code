#
# Description: Cette méthode permet de récupérer le JSON décrivant l'infrastructure depuis l'url GIT. Je JSON ainsi parser est placer dans
# une variable qui reste disponible tout au long du provisionning de la VM.
#
require 'net/http'
require 'net/https'
require 'zlib'

def parseJSONhttp(url)
  cache_file='/tmp/JSON.cache'
  cache=File.stat cache_file rescue nil
  result=nil
  if cache.nil? || cache.mtime < 20.minutes.ago
    e0_log("parseJSONhttp","info"," not using cache file")
    uri=URI(url)
    cmd="curl -k --request GET --header '#{uri.user}:#{uri.password}' #{url}"
    e0_log("parseJSONhttp","info"," cmd=#{cmd rescue "error"}") 
    file=%x(#{cmd})
    e0_log("parseJSONhttp","info"," json=#{file.slice(0,100) rescue "error"}...")
    json=parseJSON(file) rescue nil
    unless json.nil?
      e0_log("parseJSONhttp","info"," updating cache file with #{file.slice(0,20)}...")
      File.write(cache_file,file)
    end
  end
    result=File.read(cache_file)
     e0_log("parseJSONhttp","info"," JSON cache file=#{result.slice(0,20) }...")
#    result=JSON.parse(result)
#    $evm.log(:info,"CC- JSON from http=#{result.slice(0,20) }...")
    return result
end

def parseJSON(url)
   e0_log("parseJSON","info"," parsing json: #{url.slice(0,20)}...")

  if url.start_with?("http")
    e0_log("parseJSON","info","   reading json from http")
    json=parseJSONhttp(url)
  elsif url.start_with?("{")
      e0_log("parseJSON","info","   reading json from dialog")
      json=url
  else
      e0_log("parseJSON","info","   reading json from file")
      json=File.read url
  end
  e0_log("parseJSON","info"," JSON string=#{json.slice(0,20) rescue "VIDE"}")
  return JSON.parse(json) rescue nil
end

json_url = $evm.object['jsonUrl']
e0_log("main","info"," Parsing JSON from Git")
json=parseJSON(json_url) rescue nil
if json.nil?
  e0_log("main","error"," Erreur parsing JSON")
  exit MIQ_ERROR
else
  e0_log("main","info"," JSON from git=#{json.to_s.length }")
  cjson=Zlib::Deflate.deflate(json.to_json)
  e0_log("main","info"," JSON from compressed=#{cjson.length}")
  $evm.set_state_var('infraAsJson',cjson)
  $evm.set_state_var('infraAsJson_URL',json_url)
  e0_log("main","info"," saved JSON compressed=#{$evm.get_state_var('infraAsJson').length}")
end
  
#$evm.log(:info, "Mike- #{$evm.get_state_var('infraAsJson')}")
