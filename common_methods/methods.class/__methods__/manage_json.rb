
require 'zlib'
require "manageiq-password"
def get_json()
    cjson=$evm.get_state_var("infraAsJson")
    djson=Zlib::Inflate.inflate(cjson) rescue cjson
    json=JSON.parse(djson) rescue nil
    return json
end

def get_value_from_key_tag_option(variable_keys)
  prov=$evm.root[$evm.root["vmdb_object_type"]]
  variable_keys.each do |base_variable_key|
    [base_variable_key,"dialog_#{base_variable_key}"].each do |variable_key|
            
      sub_value=$evm.root[variable_key] rescue nil
      $evm.log(:warn, "CC- variable_key #{variable_key} in root=#{sub_value}") unless sub_value.nil?
      return sub_value unless sub_value.nil?
      
    ##### options ####
      sub_value=prov.get_option(variable_key) rescue nil
      $evm.log(:warn, "CC- variable_key #{variable_key} in option=#{sub_value}") unless sub_value.nil?
      return sub_value unless sub_value.nil?

      sub_value=prov.options[variable_key] rescue nil
      $evm.log(:warn, "CC- variable_key #{variable_key} in option=#{sub_value}") unless sub_value.nil?
      return sub_value unless sub_value.nil?

    ##### tags
      sub_value=prov.tags(variable_key).first rescue nil
      $evm.log(:warn, "CC- variable_key #{variable_key} in tag=#{sub_value}") unless sub_value.nil?
      return sub_value unless sub_value.nil?

      sub_value=prov.get_tag(variable_key) rescue nil
      $evm.log(:warn, "CC- variable_key #{variable_key} in get_tag=#{sub_value}") unless sub_value.nil?
      return sub_value unless sub_value.nil?
      
    ###### dialog
      sub_value=prov.get_dialog_option(variable_key) rescue nil
      $evm.log(:warn, "CC- variable_key #{variable_key} in dialog_option=#{sub_value}") unless sub_value.nil?
      return sub_value unless sub_value.nil?
  
      sub_value=prov.options[:dialog][variable_key] rescue nil
      $evm.log(:warn, "CC- variable_key #{variable_key} in root_dialog=#{sub_value}") unless sub_value.nil?
      return sub_value unless sub_value.nil? 
    end
  end
  return nil
end

def get_node(node, json=nil, spacing="", structure=nil)
  result=nil
  json=get_json if json.nil?
  structure=json["STRUCTURE"] if structure.nil?
  struct_keys=structure[node]
     $evm.log(:warn, "#{spacing}CC- #{node}: searching #{node} in #{structure.keys} with #{json.keys}")
  if struct_keys.nil?
    $evm.log(:error, "#{spacing}    CC- #{node}: not found")
  else
    parent=struct_keys[0]
    sub_level=struct_keys[1]
    variable_key=struct_keys[2] # || "tag_0_entity"
    if parent=="nil"
      $evm.log(:info, "#{spacing}    CC- #{node}: found with #{json[node].keys}")
      return json[node]
    else
      j_values, value=get_node(parent, json, "#{spacing}  ")
      $evm.log(:warn,"#{spacing}    CC- #{node}: get_node #{parent} value = #{value} sub_level=#{sub_level} j_values = #{j_values.keys rescue "NONE"}")
      result= j_values
      unless j_values.nil?
        result=result[value] unless value.blank?
        result= result[sub_level] unless sub_level.nil?
      end
      value=get_value_from_key_tag_option(variable_key)
      $evm.log(:warn,"#{spacing}    CC- #{node}: node #{node} value = #{value rescue nil}")
    end
  end
  $evm.log(:warn,"#{spacing}    CC- #{node}: node #{node} result = #{result.keys rescue nil}")
  return result, value
end

def get_attribute(attribute, json=nil, structure=nil)
  json=get_json if json.nil?
  structure=json["STRUCTURE"] if structure.nil?
  $evm.log(:info, "################################ CC- searching #{attribute} based on #{structure}")
  structure.keys.each do |node|
    $evm.log(:info, "******CC- searching #{attribute} in node #{node}")
    j_values, value=get_node(node, json, "", structure)
    j_node=j_values[value] rescue []
    att_value=j_node[attribute] rescue nil
    att_value=MiqPassword.try_decrypt(att_value)
    $evm.log(:info, "CC- #{attribute} in node #{node} = #{att_value}")
    return att_value unless att_value.nil?
  end
end
