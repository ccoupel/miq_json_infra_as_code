begin
    require 'rubygems'
    require 'net/ldap'
  require 'manageiq-password'
  # call_ldap
    def search_vm_dn(prov, vmname)
=begin
      entity=prov.get_tag(:entity)
      ad_all=JSON.parse($evm.object["ad_all_#{entity}"])
    
      servernames = prov.get_option(:dns_servers).split(",") rescue []
      bindUser=ad_all["user_cn"]
    #username=$evm.object["ad_user_#{entity}"]
      password=$evm.object.decrypt("AD_passwd_#{entity}")
      root_forest=$evm.object["AD_root_forest"]
    
      basedn=root_forest
$evm.log(:info," CC- entity=#{entity} ad_user=#{bindDN} ou=#{basedn} servers=#{servernames}")      
#    basedn="CN=Computers,DC=pr-eu,DC=pernod-ricard,DC=group"
      bindDN=bindUser+","+root_forest
=end

    computer_dn = nil
      @servernames.each do |servername|   
        $evm.log(:info, "AD definition: ldap://#{bindDN}@#{servername} ou=#{basedn}")
    # setup authentication to LDAP
        ldap = Net::LDAP.new :host => servername, :port => 389,
          :auth => {
          :method => :simple,
#        :username => "cn=#{username}, cn=users, #{basedn}",
          :username => @bindDN,
          :password => @ad_password
        }

    # Search LDAP for computername
        $evm.log(:info, "Searching LDAP server: #{servername} base_dn: #{@base_dn} for computer: #{vmname}")
        filter = Net::LDAP::Filter.eq("cn", vmname)
        result=ldap.search(:base => @base_dn, :filter => filter)
        $evm.log(:info,"CC search result= #{result}")
        ldap.search(:base => @base_dn, :filter => filter) {|entry| $evm.log(:info,"serch result=#{entry}");computer_dn = entry.dn }
        break unless computer_dn.nil?
      end
    computer_dn
  end

  
  def remove_ldap(prov, vmname, computer_dn)
=begin
  ad_all=get_attribute("AD")
  raise ("NO ADD DEFINITION FOUND") if ad_all.nil?
  dns_domain=ad_all["Domain"]
  ad_password=MiqPassword::decrypt(ad_all["Password"])
  ad_user=ad_all["Login"]
    basedn=ad_all["ou"]+", "+ad_all["base_dc"] 
    bindDN=ad_all["user_cn"]+", "+ad_all["base_dc"]
    servernames = prov.get_option(:dns_servers).split(",") rescue []

      entity=prov.get_tag(:entity)
      ad_all=JSON.parse($evm.object["ad_all_#{entity}"])
 
      bindUser=ad_all["user_cn"]
    #username=$evm.object["ad_user_#{entity}"]
      password=$evm.object.decrypt("AD_passwd_#{entity}")
      root_forest=$evm.object["root_forest"]
    
      basedn=ad_all["ou"]+","+root_forest
      
#    basedn="CN=Computers,DC=pr-eu,DC=pernod-ricard,DC=group"
      bindDN=bindUser+","+root_forest
=end
$evm.log(:info," CC- ad_user=#{@bindDN}:#{@ad_password} dn=#{computer_dn} servers=#{@servernames}")

      @servernames.each do |servername|   
        $evm.log(:info, "AD definition: ldap://#{@bindDN}@#{servername} ou=#{@basedn}")
    # setup authentication to LDAP
        ldap = Net::LDAP.new :host => servername, :port => 389,
          :auth => {
          :method => :simple,
#        :username => "cn=#{username}, cn=users, #{basedn}",
          :username => @bindDN,
          :password => @ad_password
        }

      $evm.log(:info, "Deleting computer_dn from LDAP")
      ldap.delete(:dn => computer_dn)
      result = ldap.get_operation_result
      if result.code.zero?
        $evm.log(:info, "Successfully deleted computer_dn: #{computer_dn} from LDAP Server")
        break
      else
        $evm.log(:warn, "Failed to delete computer_dn: #{computer_dn} from LDAP Server")
        $evm.log(:info, "Result Calling ldap:<#{result}>")
      end
    end
  end
  
  $evm.log(:info, "CC- unregistering LDAP #{$evm.root['vm']}")

    vm = $evm.root['vm']
    prov=vm.miq_provision
    vmname = vm.name.upcase

  
  #raise "$evm.root['vm'] not found" if vm.nil?
  $evm.log(:info, "Found VM:<#{vmname}>")

  ad_all=get_attribute("AD")
  raise ("NO ADD DEFINITION FOUND") if ad_all.nil?
  @dns_domain=ad_all["Domain"]
  @ad_password=MiqPassword::decrypt(ad_all["Password"])
  @ad_user=ad_all["Login"]
  @basedn=ad_all["ou"]+", "+ad_all["base_dc"] 
  @bindDN=ad_all["user_cn"]+", "+ad_all["base_dc"]
  @servernames = prov.get_option(:dns_servers).split(",") rescue []  
  
  computer_dn=search_vm_dn(prov, vmname)
  raise "no DN found for #{vmname}" if computer_dn.nil?
  result = remove_ldap(prov, vmname, computer_dn)
        
    $evm.root['ae_result']         = "ok"
    $evm.root['ae_retry_interval'] = 20.seconds
  
rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "ok"
    $evm.root['ae_retry_interval'] = 20.seconds
    exit MIQ_OK
end  
