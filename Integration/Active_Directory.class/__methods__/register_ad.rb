#
# Description: <Method description here>
#

@debug=true
begin
  require 'manageiq-password'
if $evm.root["vm"].nil?
  prov = $evm.root['miq_provision']
  vmtargetname = prov.get_option(:vm_target_name)
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "AD variables 1 : Prov=#{prov}, vmtargetname=#{vmtargetname}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
else
  prov = $evm.root["vm"]
  vmtargetname = $evm.root["name"]
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "AD variables 2 : Prov=#{prov},  vmtargetname=#{vmtargetname}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
end
###################### definition des elements de l'AD

    entity=prov.get_tag(:entity)
  ad_all=get_attribute("AD")
  $evm.log(:info,"CC- AD=#{ad_all}")
  raise ("NO ADD DEFINITION FOUND") if ad_all.nil?
  dns_domain=ad_all["Domain"]
  ad_password=MiqPassword::decrypt(ad_all["Password"])

  ad_user=ad_all["Login"]
    basedn=ad_all["ou"]+", "+ad_all["base_dc"] 
    bindDN=ad_all["user_cn"]+", "+ad_all["base_dc"]
    
prov.set_option(:dns_domain,  "#{dns_domain}")
prov.set_option(:dns_suffixes, "#{dns_domain}")
prov.set_option(:sysprep_domain_name,"#{dns_domain}")

prov.set_option(:sysprep_domain_password,"#{ad_password}")
  prov.set_option(:ad_domain_password,"#{ad_password}")

prov.set_option(:sysprep_domain_admin,"#{ad_user}")
  
  
$evm.log(:info,"CC- AD entity=#{entity} => #{dns_domain} #{ad_user}")


    $evm.root['ae_result']         = "ok"
    $evm.root['ae_retry_interval'] = 40.seconds
  
rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "retry"
    $evm.root['ae_retry_interval'] = 20.seconds
    exit MIQ_OK
end  
