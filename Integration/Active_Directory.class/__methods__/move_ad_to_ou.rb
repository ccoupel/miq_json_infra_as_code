#
# Description: Wait until the VM is registered in AD and then move it to the requested ou
#
begin
    require 'rubygems'
    require 'net/ldap'
require 'manageiq-password'
  # call_ldap
  
  def search_vm_dn(prov, vmname)
    computer_dn = nil
      @servernames.each do |servername|  
        ldap_servername=servername.strip
        $evm.log(:info, "AD definition: ldap://#{@bindDN}@#{ldap_servername} ")
    # setup authentication to LDAP
        ldap = Net::LDAP.new :host => ldap_servername, :port => 389,
          :auth => {
          :method => :simple,
#        :username => "cn=#{username}, cn=users, #{basedn}",
          :username => @bindDN,
          :password => @ad_password
        }

    # Search LDAP for computername
        $evm.log(:info, "Searching LDAP server: #{ldap_servername} base_dc: #{@base_dc} for computer: #{vmname}")
        filter = Net::LDAP::Filter.eq("cn", vmname)
        basedc="#{@base_dc}"
        result=ldap.search(:base => basedc, :filter => filter)
        $evm.log(:info,"CC- search #{vmname} in #{basedc} result1= #{result}")
        basedn="cn=computers,#{basedc}"
        result=ldap.search(:base => basedn, :filter => filter) if result.blank?
        $evm.log(:info,"CC- search #{vmname} in #{basedc} result1= #{result}") if result.blank?
        
        result.each {|entry| $evm.log(:info,"serch result=#{entry}");computer_dn = entry.dn } unless result.nil?
        break unless computer_dn.nil?
      end
    computer_dn
  end
  
  def move_ldap(prov, vmname, computer_dn)
      new_rdn="cn=#{vmname}"
      new_dn="#{new_rdn}, #{@oudn}"
    $evm.log(:info, "vm #{vmname} found with dn: #{computer_dn}")
    @servernames.each do |servername|   
      $evm.log(:info, "AD moving: ldap://#{@bindDN}@#{servername} dn=#{computer_dn} to new_dn=#{new_dn}")
    # setup authentication to LDAP
      ldap = Net::LDAP.new :host => servername, :port => 389,
      :auth => {
        :method => :simple,
#        :username => "cn=#{username}, cn=users, #{basedn}",
        :username => @bindDN,
        :password => @ad_password
      }

      $evm.log(:info, "moving computer_dn from #{computer_dn} to #{new_dn}")
      ldap.rename(:olddn => computer_dn, :newrdn =>new_rdn, delete_attributes: true, :new_superior => @oudn)
      result = ldap.get_operation_result
      if result.code.zero?
        $evm.log(:info, "Successfully moved computer_dn: #{computer_dn} to #{new_dn}")
        break
      else
        $evm.log(:warn, "Failed to move computer_dn: #{computer_dn} to #{new_dn}")
        $evm.log(:error, "Result Calling ldap:<#{result}>")
        raise "Failed to move computer_dn: #{computer_dn} to #{new_dn}: Result Calling ldap:<#{result}>"
      end
    end
  end
  
 def call_ldap(prov, vmname)
   computer_dn=search_vm_dn(prov, vmname)
   if computer_dn.nil?
     $evm.log(:info,"CC- #{vmname} not yet registered in AD : #{$evm.root["ae_state_retries"]}/#{$evm.root["ae_state_max_retries"]}")
     #$evm.root.attributes.each{|k,v| $evm.log(:info,"ROOT[#{k}]=#{v}")}
     return false
   else
     move_ldap(prov,vmname, computer_dn)
     return true
   end
 end
  
  $evm.log(:info, "CC- moving LDAP #{$evm.root['vm']}")

if $evm.root["vm"].nil?
  prov = $evm.root['miq_provision']
  vm=prov.destination
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "AD variables 1 : Prov=#{prov}, vmtargetname=#{vm}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
else
  vm = $evm.root["vm"]
  prov=vm.miq_provision
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "AD variables 2 : Prov=#{prov},  vmtargetname=#{vm}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
end
    vmname = vm.name.upcase

  
  #raise "$evm.root['vm'] not found" if vm.nil?
  $evm.log(:info, "Found VM:<#{vmname}>")

      @entity=prov.get_tag(:entity)
=begin
      @ad_all=JSON.parse($evm.object["ad_all_#{@entity}"])
    
      @servernames = prov.get_option(:dns_servers).split(",") rescue []
      @root_forest=$evm.object["AD_root_forest"]
      @base_dc=@ad_all["base_dc"] 
      @basedn="#{@root_forest}"
      @basedn="#{@base_dc},#{@root_forest}" unless @base_dc.blank?
  
      @oudn="#{@ad_all["ou"]},#{@basedn}"
      @bindDN="#{@ad_all["user_cn"]},#{@basedn}"
      @password=$evm.object.decrypt("AD_passwd_#{@entity}")
=end
    @ad_all=get_attribute("AD")
  raise ("NO ADD DEFINITION FOUND") if @ad_all.nil?
  @dns_domain=@ad_all["Domain"]
  @ad_password=MiqPassword.decrypt(@ad_all["Password"])
  @ad_user=@ad_all["Login"]
  @base_dc=@ad_all["base_dc"]
  @ou=@ad_all["ou"]
    @oudn=@ou+", "+@ad_all["base_dc"] 
    @bindDN=@ad_all["user_cn"]+", "+@ad_all["base_dc"]
        @servernames = prov.get_option(:dns_servers).split(",") rescue []

      $evm.log(:info," CC- entity=#{@entity} ad_user=#{@bindDN} ou=#{@oudn} base_dc=#{@base_dc} servers=#{@servernames}")
  
  result = call_ldap(prov, vmname)
        
  $evm.root['ae_result']         = "ok" # exit if vmname found and moved
  $evm.root['ae_result']         = "retry"  unless result # retry if vmname not found in AD
  $evm.root['ae_retry_interval'] = 60.seconds
  
rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "error"
    $evm.root['ae_retry_interval'] = 60.seconds
    exit MIQ_OK
end  
