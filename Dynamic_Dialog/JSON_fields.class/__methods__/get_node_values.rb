require 'zlib'

begin
  
dialog_field = $evm.object
=begin
# sort_by: value / description / none
dialog_field["sort_by"] = "value"

# sort_order: ascending / descending
dialog_field["sort_order"] = "ascending"

# data_type: string / integer
dialog_field["data_type"] = "string"

# required: true / false
dialog_field["required"] = "true"

$evm.root.attributes.each{|k,v| $evm.log(:info,"CC- ROOT[#{k}]=#{v}")}
  $evm.object.attributes.each{|k,v| $evm.log(:info,"CC- OBJECT[#{k}]=#{v}")}

  cjson=$evm.get_state_var("infraAsJson")
    djson=Zlib::Inflate.inflate(cjson) rescue cjson
    json=JSON.parse(djson)

  json=get_json

  if json.nil?
    e0_log("dialog","error", "CC- erreur reading JSON: using default values")

      values = {"ERROR Reading JSON" => "ERROR Reading JSON"}
      
  else
=end
    values={}
    node=$evm.root["node"]
j_values, value=get_node(node)    
      j_values.keys.each do |k| 
          $evm.log(:info, "    CC- adding key #{k} => #{j_values[k]["description"] rescue k}")
          values[k]=(j_values[k]["description"] || k) rescue k
      end rescue nil 

#  end
  dialog_field["values"]=values
  $evm.object['visible'] = (values.count >= 0)
  $evm.object['read_only'] = (values.count <= 1)
  $evm.object["default_value"]=$evm.object["values"].first.first rescue nil

#  get_attribute("AD")
  
rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "error"
    $evm.root['ae_retry_interval'] = 20.seconds
    exit MIQ_OK
end  
